/*Copyright (C) 
 * 2019 - eda1 dot fiunam at yahoo dot com dot mx
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include <iostream>
// para cin, cout, cerr. (leer del teclado e imprimir en la pantalla)

#include <string>
// para declarar y procesar cadenas

#include "Vertex.hpp"
#include "Graph.hpp"

void bfs( Graph& graph, std::string start )
{
	auto vertices = graph.get_vertices();
	// nos traemos al conjunto de vértices del grafo

	for( auto v : vertices ){
		std::string key   = v.first;
		Vertex      value = v.second;

		value.set_color( Vertex::Colors::BLACK );
		// el vértice no ha sido descubierto ni visitado

		value.set_distance( 0 );

		value.set_predecesor( "NA" );
	}

#if 1 

	Vertex start_vertex = graph.get_vertex( start );

	start_vertex.set_color( Vertex::Colors::GRAY );
	start_vertex.set_distance( 0 );
	start_vertex.set_predecesor( "Nil" );


	std::list<Vertex> queue;

	queue.push_back( start_vertex );

	while( not queue.empty() ){

		Vertex working_vertex = queue.front(); queue.pop_front();
		std::cerr << "WV: ";
		working_vertex.print();

		auto v = working_vertex.get_neighbors();
		// v es una LISTA de vértices, no es un vértice simple

		for( Vertex w : v ){
			if( w.get_color() == Vertex::Colors::BLACK ){

				w.set_color( Vertex::Colors::GRAY );
				// el vértice ha sido descubierto

				w.set_distance( working_vertex.get_distance() + 1 );

				w.set_predecesor( working_vertex.get_name() );

				queue.push_back( w );

				w.print();

			}
		}

		working_vertex.set_color( Vertex::Colors::WHITE );
		// el vértice ha sido visitado
	}
#endif  

	for( auto& v : graph.get_vertices() ){
		v.second.print();
	}
}

int main()
{
	std::array<std::string, 11> vertices = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K" };
	// std::array son como los arreglos estáticos nativos de C, [], pero con esteroides.
	// También existen los arreglos dinámicos: std::vector

	std::array<std::string, 10> edges = { "AE", "AB", "BC", "BF", "DH", "EI", "FI", "GJ", "GK", "HK" };
	// primero escribí las conexiones, luego las conté, y finalmente, escribí el '10'.
	// Como este ejemplo es de un grafo no dirigido, resulta que "AE" es lo mismo que
	// "EA", por eso no es necesario duplicar la conexión.

	Graph g;

	for( auto& v : vertices ){ g.add_vertex( Vertex( v ) ); }

	for( auto& e : edges ){ g.add_edge( e.substr( 0, 1 ), e.substr( 1, 1 ) ); }
	// .substr( a_partir_de_dónde, cuántos_queremos_leer )

//	g.print();
//
	bfs( g, "A" );
}
